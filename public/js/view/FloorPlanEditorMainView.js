function init(){

 	return new FloorPlanEditorMainView();
 }

function FloorPlanEditorMainView() {

    this.bp1='';
    this.bp2='';

}

FloorPlanEditorMainView.prototype.addAppsOpenButtonListener = function() {

    if ($("#app_dropdown_floorPlanEditor").length) {
     
    	//when a user clicks on the app name in the apps drop down
    	$('#app_dropdown_floorPlanEditor').off().on('click', $.proxy(function(){this.openFloorPlanEditor();}, this));

    }else{

    	//try adding it again (timeout to stop js from blowing it's stack)
    	setTimeout($.proxy(function(){ this.addAppsOpenButtonListener; console.log('called')}, this), 250);
    }



}

//This method SHOWS the App
FloorPlanEditorMainView.prototype.openFloorPlanEditor = function() {

    console.log('openFloorPlanEditor');

    //create a new instance of the floorplan view
    this.newEditFloorPlan = new FloorPlanViewerMainView();

	//change the type
    this.newEditFloorPlan.viewerType='editorMode';
    this.newEditFloorPlan.tabTitle='Floorplan Editor';

    //now open it
    this.newEditFloorPlan.showApp();

    //add the editor buttons
    $('#FPVEditButtonsContainer_'+this.newEditFloorPlan.tabID).html(Handlebars.templates.FloorPlanEditorButtons());

    //add event listeners for edit butons
    $('.floorplanEditorButtonCells').off().on('click', $.proxy(function(){this.editButtonClicked(event);}, this));
    

}

//This method hides the App
FloorPlanEditorMainView.prototype.hideApp = function() {

    console.log('Hide Boiler Plate!');

}

FloorPlanEditorMainView.prototype.loadTemplate = function() {

    //$('#appEntryPointOne').html(Handlebars.templates.YetiAppTemplateBolierPlate());
    console.log('FloorPlanEditor template Loaded!');

}

// action when one of the edit buttons are clicked
FloorPlanEditorMainView.prototype.editButtonClicked = function(event){

    //get the button type
    this.buttonType = event.currentTarget.dataset.buttonAction;

    //if this is NOT selected then do this
    if(!$('[data-button-action = "'+this.buttonType+'"]').hasClass("floorplanEditorButtonCellsSelected")){

        //add this class
        $('[data-button-action = "'+this.buttonType+'"]').addClass('floorplanEditorButtonCellsSelected');

        //if this is the SELECT button
        if(this.buttonType=='select'){

            this.selectButtonOn();
        }

        //if this is the MOVE button
        if(this.buttonType=='move'){

            this.moveButtonOn();
        }

    //looks like a deselect!
    }else{

        //remove this class
        $('[data-button-action = "'+this.buttonType+'"]').removeClass('floorplanEditorButtonCellsSelected');

        //if this is the SELECT button
        if(this.buttonType=='select'){

            this.selectButtonOff();
        }

        //if this is the MOVE button
        if(this.buttonType=='move'){

            this.moveButtonOff();
        }

    }

}

// add event listeners dragging paths around
FloorPlanEditorMainView.prototype.addDragger = function(){

    //add a PLHandle eventlistener
    $('path').on('mousedown', $.proxy(function(event){

        event.preventDefault();

        var groupID = event.target.parentElement.id;

        //need to work out how the drawing is scaled
        var SVGScale = $('#svgID_'+this.newEditFloorPlan.tabID).children().next('g').children();
        this.SVGScaleX = Number(SVGScale[0].transform.baseVal[0].matrix.a);
        this.SVGScaleY = Number(SVGScale[0].transform.baseVal[0].matrix.d);

        //get this
        var plHandle = $(event.target).attr('plhandle');

        //move the text to this SVG group
        if(Nexus.GlobalLocationCollection.pLHandleMatrix.hasOwnProperty(plHandle)){

            //remove the existing one
            Nexus.GlobalLocationCollection.pLHandleMatrix[plHandle].removeLabelAndIDFromFloorPlan();

            //re-add it into the group
            Nexus.GlobalLocationCollection.pLHandleMatrix[plHandle].addLabelAndIDToFloorPlan(this.newEditFloorPlan.tabID, groupID);

        }

        //get start pos
        var sX = event.offsetX;
        var sY = event.offsetY;

        // Find your root SVG element
        this.SVGPoint = $('#svgID_'+this.newEditFloorPlan.tabID)[0].createSVGPoint();

        this.SVGPoint.x = event.clientX; 
        this.SVGPoint.y = event.clientY;
        this.SVGStartPoint = this.SVGPoint.matrixTransform($('.svg-pan-zoom_viewport')[0].getCTM().inverse());

        //get these
        this.lastXPos = $('#'+groupID ).attr('translate-x');
        this.lastYPos = $('#'+groupID ).attr('translate-y');
        
        //if this is the first time then they will be undefined
        if (this.lastXPos=='undefined'|| this.lastXPos===undefined) {

            this.lastXPos=0;
            this.lastYPos=0;

        }

        //add a PLHandle eventlistener
        $(document).off('mousemove').on('mousemove', $.proxy(function(event){

            //get these
            this.SVGCurrent = $('#svgID_'+this.newEditFloorPlan.tabID)[0].createSVGPoint();
            this.SVGCurrent.x = event.clientX; 
            this.SVGCurrent.y = event.clientY;
            this.SVGCurrentPos = this.SVGCurrent.matrixTransform($('.svg-pan-zoom_viewport')[0].getCTM().inverse());

            //do the math!
            if(this.SVGScaleX<0){

                cX = Number(this.lastXPos)+(this.SVGStartPoint.x-this.SVGCurrentPos.x);

            }else{

                cX = Number(this.lastXPos)+(this.SVGCurrentPos.x-this.SVGStartPoint.x);

            }

            if(this.SVGScaleY<0){

                cY = Number(this.lastYPos)+(this.SVGStartPoint.y-this.SVGCurrentPos.y);

            }else{

                cY = Number(this.lastYPos)+(this.SVGCurrentPos.y-this.SVGStartPoint.y);
            }
            
            //get moving
            $('#'+groupID ).attr({"transform":"translate("+cX+","+cY+")"});

            //update this
            $('#'+groupID ).attr({'translate-x':cX}).attr({'translate-y':cY});

            //stop moving
            $('path').on('mouseup', $.proxy(function(event){

                $(document).off('mousemove');

            }, this));

        }, this));

    }, this));

}

// remove event listeners dragging paths around
FloorPlanEditorMainView.prototype.removeDragger = function(){

    $('path').off('mousedown');
    $('path').off('mouseup');

}

//switch select mode ON
FloorPlanEditorMainView.prototype.selectButtonOn = function(event){

    //disable these
    this.newEditFloorPlan.svgPanZoomInstances.disablePan();
    //this.newEditFloorPlan.svgPanZoomInstances.disableZoom();


}

//switch select modeOFF
FloorPlanEditorMainView.prototype.selectButtonOff = function(event){

    //enable these
    this.newEditFloorPlan.svgPanZoomInstances.enablePan();
    //this.newEditFloorPlan.svgPanZoomInstances.enableZoom();


}

//switch MOVE mode ON
FloorPlanEditorMainView.prototype.moveButtonOn = function(event){

    //disable these
    this.newEditFloorPlan.svgPanZoomInstances.disablePan();
    // this.newEditFloorPlan.svgPanZoomInstances.disableZoom();

    //add these event listeners
    this.addDragger();


}

//switch MOVE modeOFF
FloorPlanEditorMainView.prototype.moveButtonOff = function(event){

    //enable these
    this.newEditFloorPlan.svgPanZoomInstances.enablePan();
    // this.newEditFloorPlan.svgPanZoomInstances.enableZoom();

    //remove these event listeners
    this.removeDragger();

}


























