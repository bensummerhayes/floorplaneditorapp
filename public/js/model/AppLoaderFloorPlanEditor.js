
function appLoadList(){


	var appLoadList={

		// Primary Load List, add the scripts you want to front load here (these will load in order!!)
		primaryLoadList:[

			 "../../floorplaneditorapp/public/js/view/FloorPlanEditorMainView.js"
			,"../../floorplaneditorapp/public/html/templates/preCompiledTemplates/FloorPlanEditorTemplate.min.js"
			,"../../floorplaneditorapp/public/styles/css/floorPlanEditorStyle.css"

		],

		// Secondary Load List, add the scripts you want to load AFTER the Primary list (these will load in order!!)
		secondaryLoadList:[

			// "public/js/view/BoilerPlate.js"

		],

		//add in here the functions you would like to ba called AFTER the PRIMARY load list has loaded
		primaryCallbackList:{

			addFloorPlanEditorEventListeners: function(Nexus){Nexus.FloorPlanEditorMainView.addAppsOpenButtonListener()}

		},

		//add in here the functions you would like to ba called AFTER the SECONDARY load list has loaded
		secondaryCallbackList:{

			// consoleLog: function(Nexus){console.log('App BP CB TWO')}

		}


	}

	return appLoadList;

}

//call this
var appLoaderObject = appLoadList();

//add these to the nexus
for (i = 0; i < appLoaderObject.primaryLoadList.length; i++) { 
    Nexus.primaryLoadList.push(appLoaderObject.primaryLoadList[i]);
}
for (i = 0; i < appLoaderObject.secondaryLoadList.length; i++) { 
    Nexus.secondaryLoadList.push(appLoaderObject.secondaryLoadList[i]);
}
for(key in appLoaderObject.primaryCallbackList){

	Nexus.primaryCallbackList[key]=appLoaderObject.primaryCallbackList[key];
}
for(key in appLoaderObject.secondaryCallbackList){

	Nexus.secondaryCallbackList[key]=appLoaderObject.secondaryCallbackList[key];
}
